<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use yii\base\Model;
use app\Classes\PHPExcel;

/**
 * Description of ExcelExport
 *
 * @author mezinn
 */
class ExcelExport extends Model {

    public $summary;

    public function export() {
        $document = new \PHPExcel;
        $sheet = $document->setActiveSheetIndex(0);
        $columns = ['№', 'Название проверки', 'Статус', ' ', 'Текущее состояние'];
        $row = 0;
        $this->renderHead($columns, $sheet, $row + 1);
        foreach ($this->summary as $index => $item) {
            $this->renderDummy($columns, $sheet, $row += 2);
            $this->renderItem($item, $sheet, ++$row, $index + 1);
        }
        $this->centered($sheet, 10, 30);
        $this->autoSize($sheet);

        $objWriter = \PHPExcel_IOFactory::createWriter($document, 'Excel5');
        $objWriter->save("export.xls");
    }

    protected function fillColor($sheet, $column, $line, $color) {
        return $sheet->getStyleByColumnAndRow($column, $line)
                        ->getFill()
                        ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setRGB($color);
    }

    protected function renderHead($columns, $sheet, $row) {
        $tmpColumn = 0;
        foreach ($columns as $column) {
            $this->fillColor($sheet, $tmpColumn, $row, 'A2C4C9');
            $sheet->setCellValueByColumnAndRow($tmpColumn, $row, $column);
            $tmpColumn++;
        }
    }

    protected function renderDummy($columns, $sheet, $row) {
        $tmpColumn = 0;
        foreach ($columns as $column) {
            $this->fillColor($sheet, $tmpColumn, $row, 'EFEFEF');
            $sheet->setCellValueByColumnAndRow($tmpColumn, $row, '');
            $tmpColumn++;
        }
    }

    protected function renderItem($item, $sheet, $row, $index) {
        $tmpCol = 0;
        $sheet->setCellValueByColumnAndRow($tmpCol, $row, $index);
        $sheet->mergeCellsByColumnAndRow($tmpCol, $row, $tmpCol, $row + 1);

        $sheet->setCellValueByColumnAndRow($tmpCol + 1, $row, $item['name']);
        $sheet->mergeCellsByColumnAndRow($tmpCol + 1, $row, $tmpCol + 1, $row + 1);

        $this->fillColor($sheet, $tmpCol + 2, $row, ($item['status'] == 'Ок' ? '00FF00' : 'E06666'));
        $sheet->setCellValueByColumnAndRow($tmpCol + 2, $row, $item['status']);
        $sheet->mergeCellsByColumnAndRow($tmpCol + 2, $row, $tmpCol + 2, $row + 1);

        $sheet->setCellValueByColumnAndRow($tmpCol + 3, $row, 'Состояние');
        $sheet->setCellValueByColumnAndRow($tmpCol + 3, $row + 1, 'Рекомендации');

        $sheet->setCellValueByColumnAndRow($tmpCol + 4, $row, $item['state']);
        $sheet->setCellValueByColumnAndRow($tmpCol + 4, $row + 1, $item['recommends']);
        $this->fontSize($sheet, 10, $row);
        $this->fontSize($sheet, 10, $row + 1);
    }

    protected function centered($sheet, $columns, $rows) {
        for ($i = 0; $i < $rows; $i++) {
            for ($j = 0; $j < $columns; $j++) {
                $sheet->getStyleByColumnAndRow($j, $i)->getAlignment()->setHorizontal(
                        \PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $sheet->getStyleByColumnAndRow($j, $i)->getAlignment()->setVertical(
                        \PHPExcel_Style_Alignment::VERTICAL_CENTER);
            }
        }
    }

    protected function autoSize($sheet) {
        foreach (range('A', 'E') as $columnID) {
            $sheet->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }
    }

    protected function fontSize($sheet, $font, $row) {
        foreach (range('A', 'E') as $columnID) {
            $sheet->getStyle($columnID . $row)->getFont()->setSize($font);
            $sheet->getStyle($columnID . $row)->getAlignment()->setWrapText(true);
            $sheet->getRowDimension($columnID . $row)->setRowHeight(300);
            $this->borders($sheet, $columnID . $row);
        }
    }

    protected function borders($sheet, $cell) {
        $bundle = ['style' => \PHPExcel_Style_Border::BORDER_THICK, 'color' => array('rgb' => 'cccccc')];
        $border_style = ['borders' => ['right' => $bundle, 'left' => $bundle, 'top' => $bundle, 'bottom' => $bundle]];
        $sheet->getStyle($cell)->applyFromArray($border_style);
    }

}
