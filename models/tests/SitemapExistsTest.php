<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\tests;

use app\models\BasicTest;

/**
 * Description of SitemapExistsTest
 *
 * @author mezinn
 */
class SitemapExistsTest extends BasicTest {

    public function exec($content, $statusCode) {
        $content = ($statusCode == 200) && $this->hasSitemap($content);
        $this->name = 'Проверка указания директивы Sitemap';
        $this->status = $content ? 'Ок' : 'Ошибка';
        $this->state = $content ? 'Директива Sitemap указана' : 'В файле robots.txt не указана директива Sitemap';
        $this->recommends = $content ? 'Доработки не требуются' : 'Программист: Добавить в файл robots.txt директиву Sitemap.';
    }

    protected function hasSitemap($content) {
        return substr_count(strtolower($content), strtolower('SITEMAP')) > 0;
    }

}
