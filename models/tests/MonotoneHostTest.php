<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\tests;

use app\models\BasicTest;

/**
 * Description of MonotoneHost
 *
 * @author mezinn
 */
class MonotoneHostTest extends BasicTest {

    public function exec($content, $statusCode) {
        $content = ($statusCode == 200) && $this->hasHost($content);
        $this->name = 'Проверка количества директив Host, прописанных в файле';
        $this->status = $content ? 'Ок' : 'Ошибка';
        $this->state = $content ? 'В файле прописана 1 директива Host' : 'В файле прописано несколько директив Host';
        $this->recommends = $content ? 'Доработки не требуются' : 'Программист: Директива Host должна быть указана в файле толоко 1 раз. Необходимо удалить все дополнительные директивы Host и оставить только 1, корректную и соответствующую основному зеркалу сайта';
    }

    protected function hasHost($content) {
        return substr_count(strtolower($content), strtolower('HOST')) == 1;
    }

}
