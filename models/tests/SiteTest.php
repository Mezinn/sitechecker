<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\tests;

use yii\base\Model;
use app\models\Report;
use app\models\tests\HostExistsTest;
use app\models\tests\MonotoneHostTest;
use app\models\tests\RobotsExistsTest;
use app\models\tests\SitemapExistsTest;
use app\models\tests\RobostStatusCodeTest;
use app\models\tests\RobotsCorrectSizeTest;

/**
 * Description of SiteTest
 *
 * @author mezinn
 */
class SiteTest extends Model {

    public $content;
    public $statusCode;

    public function exec() {
        $report = new Report(['content' => $this->content, 'statusCode' => $this->statusCode]);
        $report->add(new RobotsExistsTest());
        $report->add(new HostExistsTest());
        $report->add(new MonotoneHostTest());
        $report->add(new RobotsCorrectSizeTest());
        $report->add(new SitemapExistsTest());
        $report->add(new RobostStatusCodeTest());
        return $report->exec();
    }

}
