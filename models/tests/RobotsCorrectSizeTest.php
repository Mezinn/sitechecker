<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\tests;

use app\models\BasicTest;

/**
 * Description of RobotsCorrectTest
 *
 * @author mezinn
 */
class RobotsCorrectSizeTest extends BasicTest {

    public function exec($content, $statusCode) {
        $size = strlen($content);
        $content = ($statusCode == 200) && $size < 32768;
        $this->name = 'Проверка размера файла robots.txt';
        $this->status = $content ? 'Ок' : 'Ошибка';
        $this->state = $content ? "Размер файла robots.txt составляет  $size, что находится в пределах допустимой нормы" : "Размера файла robots.txt составляет $size, что превышает допустимую норму";
        $this->recommends = $content ? 'Доработки не требуются' : 'Программист: Максимально допустимый размер файла robots.txt составляем 32 кб. Необходимо отредактировть файл robots.txt таким образом, чтобы его размер не превышал 32 Кб';
    }

}
