<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\tests;

use app\models\BasicTest;

/**
 * Description of RobostStatusCodeTest
 *
 * @author mezinn
 */
class RobostStatusCodeTest extends BasicTest {

    public function exec($content, $statusCode) {
        $content = ($statusCode == 200);
        $this->name = 'Проверка кода ответа сервера для файла robots.txt';
        $this->status = $content ? 'Ок' : 'Ошибка';
        $this->state = $content ? 'Файл robots.txt отдаёт код ответа сервера 200' : "При обращении к файлу robots.txt сервер возвращает код ответа ({$statusCode})";
        $this->recommends = $content ? 'Доработки не требуются' : 'Программист: Файл robots.txt должны отдавать код ответа 200, иначе файл не будет обрабатываться. Необходимо настроить сайт таким образом, чтобы при обращении к файлу robots.txt сервер возвращает код ответа 200.';
    }

}
