<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\tests;

use app\models\BasicTest;

/**
 * Description of RobotsTest
 *
 * @author mezinn
 */
class RobotsExistsTest extends BasicTest {

    public function exec($content, $statusCode) {
        $content = ($statusCode == 200) && $content;
        $this->name = 'Проверка наличия файла robots.txt';
        $this->status = $content ? 'Ок' : 'Ошибка';
        $this->state = $content ? 'Файл robots.txt присутствует' : 'Файл robots.txt отсутствует';
        $this->recommends = $content ? 'Доработки не требуются' : 'Программист: Создать файл robots.txt и разместить его на сайте.';
    }

}
