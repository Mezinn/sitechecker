<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\tests;

use app\models\BasicTest;

/**
 * Description of HostExistsTest
 *
 * @author mezinn
 */
class HostExistsTest extends BasicTest {

    public function exec($content, $statusCode) {
        $content = ($statusCode == 200) && $this->hasHost($content);
        $this->name = 'Проверка указания директивы Host';
        $this->status = $content ? 'Ок' : 'Ошибка';
        $this->state = $content ? 'Директива Host указана' : 'В файле robots.txt не указана директива Host';
        $this->recommends = $content ? 'Доработки не требуются' : 'Программист: Для того, чтобы поисковые системы знали, какая версия сайта является основных зеркалом, необходимо прописать адрес основного зеркала в директиве Host. В данный момент это не прописано. Необходимо добавить в файл robots.txt директиву Host. Директива Host задётся в файле 1 раз, после всех правил.';
    }

    protected function hasHost($content) {
        return substr_count(strtolower($content), strtolower('HOST')) > 0;
    }

}
