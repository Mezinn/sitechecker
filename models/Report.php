<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use yii\base\Model;
use BadMethodCallException;

/**
 * Description of Report
 *
 * @author mezinn
 */
class Report extends Model {

    protected $tests = [];
    public $content;
    public $statusCode;

    public function add($test) {
        array_push($this->tests, $test);
    }

    public function exec() {
        foreach ($this->tests as $test) {
            $test->exec($this->content, $this->statusCode);
        }
        return $this->tests;
    }

}
