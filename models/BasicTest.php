<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use yii\base\Model;

/**
 * Description of BasicTest
 *
 * @author mezinn
 */
class BasicTest extends Model {

    public $name;
    public $state;
    public $status;
    public $content;
    public $recommends;

    public function exec($content, $statusCode) {
        
    }

    public function getName() {
        return $this->name;
    }

    public function getRecommends() {
        return $this->recommends;
    }

    public function getState() {
        return $this->state;
    }

    public function getStatus() {
        return $this->status;
    }

}
