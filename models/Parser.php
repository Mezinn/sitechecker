<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use yii\base\Model;

/**
 * Description of Parser
 *
 * @author mezinn
 */
class Parser extends Model {

    public $host;
    public $uri;
    public $headers = [
        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Language: en-US,en;q=0.5',
        'Cache-Control: no-cache',
        'Content-Type: text/html; charset=UTF-8',
        'Referer: https://www.google.com',
        'User-Agent: Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36',
        'X-MicrosoftAjax: Delta=true'
    ];
    protected $handler;
    protected $lastContent;
    protected $lastStatusCode;

    public function __construct($config = array()) {
        parent::__construct($config);
        $this->handler = curl_init();
        curl_setopt($this->handler, CURLOPT_FAILONERROR, true);
        curl_setopt($this->handler, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->handler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->handler, CURLOPT_HTTPHEADER, $this->headers);
    }

    public function __destruct() {
        curl_close($this->handler);
    }

    public function rules() {
        return [
            [['host', 'uri'], 'required'],
            ['host', 'url', 'defaultScheme' => 'https'],
            ['uri', 'string', 'max' => 255],
        ];
    }

    public function attributeLabels() {
        return [
            'host' => 'Адрес сайта (пример: google.com)'
        ];
    }

    protected function checkHost() {
        if ($this->validate()) {
            curl_setopt($this->handler, CURLOPT_URL, $this->host);
            $this->lastContent = curl_exec($this->handler);
            $this->host = curl_getinfo($this->handler)['url'];
            $this->lastStatusCode = curl_getinfo($this->handler)['http_code'];
            return $this->lastStatusCode;
        }
    }

    protected function checkUri() {
        if ($this->validate()) {
            curl_setopt($this->handler, CURLOPT_URL, trim($this->host, '/') . $this->uri);
            $this->lastContent = curl_exec($this->handler);
            $this->lastStatusCode = curl_getinfo($this->handler)['http_code'];
            return $this->lastStatusCode;
        }
    }

    public function exec() {
        return ($this->checkHost() == 200) && ($this->checkUri() == 200);
    }

    public function getContent() {
        return $this->lastContent;
    }

    public function getStatusCode() {
        return $this->lastStatusCode;
    }

}
