<?php
/* @var $this yii\web\View */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
$this->title = 'Site checker';
?>
<style type="text/css">
    table,thead,tbody,tfoot,tr,th,td { font-family:"Calibri"; font-size:x-small }
    a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } 
    a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } 
    comment { display:none;  } 
    td{
        text-align: center;
        vertical-align: middle;
    }
    .table{
        margin-top: 10px;
    }
</style>

<div class="site-index">    
    <?php $form = ActiveForm::begin() ?>

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
            <?= $form->field($model, 'host') ?>
            <?= Html::submitButton('Проверить', ['class' => 'btn btn-success']) ?>
            <?php if (isset($summary)): ?>
                <?= Html::a('Сохранить', ['site/save'], ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>
        </div>
    </div>
    <?php if (isset($summary)): ?>
        <div class="table" id="table">
            <table cellspacing="0" border="0">
                <colgroup width="64"></colgroup>
                <colgroup width="396"></colgroup>
                <colgroup width="90"></colgroup>
                <colgroup width="112"></colgroup>
                <colgroup width="486"></colgroup>
                <tr>
                    <td style="border-top: 2px solid #cccccc; border-bottom: 2px solid #cccccc; border-left: 2px solid #cccccc; border-right: 2px solid #cccccc" height="21" align="center" valign=middle bgcolor="#A2C4C9"><b><font face="Arial" color="#000000">№</font></b></td>
                    <td style="border-top: 2px solid #cccccc; border-bottom: 2px solid #cccccc; border-left: 2px solid #cccccc; border-right: 2px solid #cccccc" align="left" valign=middle bgcolor="#A2C4C9"><b><font face="Arial" color="#000000">Название проверки</font></b></td>
                    <td style="border-top: 2px solid #cccccc; border-bottom: 2px solid #cccccc; border-left: 2px solid #cccccc; border-right: 2px solid #cccccc" align="center" valign=bottom bgcolor="#A2C4C9"><b><font face="Arial" color="#000000">Статус</font></b></td>
                    <td style="border-top: 2px solid #cccccc; border-bottom: 2px solid #cccccc; border-left: 2px solid #cccccc; border-right: 2px solid #cccccc" align="left" valign=bottom bgcolor="#A2C4C9"><font face="Arial" color="#000000"><br></font></td>
                    <td style="border-top: 2px solid #cccccc; border-bottom: 2px solid #cccccc; border-left: 2px solid #cccccc; border-right: 2px solid #cccccc" align="left" valign=bottom bgcolor="#A2C4C9"><b><font face="Arial" color="#000000">Текущее состояние</font></b></td>
                </tr>

                <?php foreach ($summary as $index => $item): ?>
                    <?= $this->render('_item', ['model' => $item, 'index' => $index + 1]) ?>
                <?php endforeach; ?>
            </table>

        </div>
    <?php endif;
    ?>
    <?php ActiveForm::end(); ?>
</div>
