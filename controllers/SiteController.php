<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Parser;
use app\models\tests\SiteTest;
use app\models\ExcelExport;

class SiteController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $model = new Parser(['uri' => '/robots.txt']);
        if ($model->load(Yii::$app->request->post())) {
            $model->exec();
            $siteTest = new SiteTest(['content' => $model->content, 'statusCode' => $model->statusCode]);
            $summary = $siteTest->exec();
            Yii::$app->session->set('summary', serialize($summary));
            return $this->render('index', ['model' => $model, 'summary' => $summary]);
        }
        return $this->render('index', ['model' => $model]);
    }

    public function actionSave() {
        if (($summary = unserialize(Yii::$app->session->get('summary')))) {
            (new ExcelExport(['summary' => $summary]))->export();
            Yii::$app->response->sendFile('export.xls')->send();
        }
    }

}
